/*
 * file: loadingmusic2_extra.as
 *
 * Extra script for general usages.
 */
/*
 * About: MIT License
 *
 * Copyright (c) 2021 Anggara Yama Putra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


/*
 * namespace: AN::LOADINGMUSIC2
 *
 * --- C++
 * namespace AN::LOADINGMUSIC2 {...}
 * ---
 */
namespace AN{namespace LOADINGMUSIC2{

/*
 * string: CONFIG_ROOT_PATH
 *
 * Configuration root path.
 */
const string CONFIG_ROOT_PATH       = "scripts";

/*
 * string: CONFIG_DEFAULT_PATH
 *
 * Default configuration file path.
 */
const string CONFIG_DEFAULT_PATH    = "loading_music/loading_music.ini";

/*
 * string: CONFIG_MAP_PATH
 *
 * Map-specific configuration directory path.
 */
const string CONFIG_MAP_PATH        = "loading_music/loading_music";

/*
 * array: DEFAULT_PLAYLIST
 *
 * hardcoded playlist.
 */
const string[] DEFAULT_PLAYLIST =
{
    "media/Half-Life17"
};

/*
 * obj: g_LoadingMusic
 *
 * Global instance of <AN::LOADINGMUSIC2::CLoadingMusic>.
 */
CLoadingMusic g_LoadingMusic();

/*
 * obj: CVAR_TRACK
 */
const CCVar CVAR_TRACK( "lm2_track",    "-1",   "Sets playback mode: <-1|0|track number|file name>", ConCommandFlag::AdminOnly, @OnCvarValueChanged );

/*
 * obj: CVAR_LOOP
 */
const CCVar CVAR_LOOP(  "lm2_loop",     0,      "Customizes loop mode playback: <0|1|2>", ConCommandFlag::AdminOnly, @OnCvarValueChanged );

/*
 * obj: CVAR_DELAY
 */
const CCVar CVAR_DELAY( "lm2_delay",    0,      "Player-connecting track play delay time: <float>", ConCommandFlag::AdminOnly, @OnCvarValueChanged );

/*
 * obj: CVAR_FLAGS
 */
const CCVar CVAR_FLAGS( "lm2_flags",    "abc",  "Track loading flags: <alphabets>", ConCommandFlag::AdminOnly, @OnCvarValueChanged );

/*
 * method: OnCvarValueChanged
 *
 * Unified callback for internal cvars.
 */
void OnCvarValueChanged (CCVar@ pCvar, const string& in szOldValue, float flOldValue)
{
    if (pCvar is null || !pCvar.HasBeenAdded())
        return;

    if (pCvar is CVAR_TRACK)
    {
        g_LoadingMusic.m_szPlaybackMode = pCvar.GetString();
    }
    else
    if (pCvar is CVAR_LOOP)
    {
        g_LoadingMusic.m_iLoopMode = LoopMode(Math.clamp( LOOP_NO, LOOP_4EVER, pCvar.GetInt() ));
    }
    else
    if (pCvar is CVAR_DELAY)
    {
        g_LoadingMusic.m_flPlayOnConnectDelay = pCvar.GetFloat();
    }
    else
    if (pCvar is CVAR_FLAGS)
    {
        g_LoadingMusic.m_szFlags = pCvar.GetString();
    }
}

/*
 * method: Initialize
 *
 * Initialize the <AN::LOADINGMUSIC2::g_LoadingMusic>.
 *
 * Arguments:
 * bCanPrecache     - Allow track files to be precached.
 */
void Initialize (const bool bCanPrecache)
{
    const string szDebugMsg = g_LibString.GetFormattedString( "Initialize(bCanPrecache=%1)...", {bCanPrecache} );
    g_LibDebug.verboselogln( szDebugMsg );

    ReadConfigFile( bCanPrecache );
    if (g_LoadingMusic.m_uiTrackCount <= 0)
    {
        // try to use hardcoded default playlist
        for (uint i = 0; i < DEFAULT_PLAYLIST.length(); i++)
            g_LoadingMusic.AddTrack( DEFAULT_PLAYLIST[i], bCanPrecache );
    }

    if (bCanPrecache)
    {
        g_LoadingMusic.PrecacheCustomTrack();
    }

    g_LoadingMusic.Start();

    g_LibDebug.logln( szDebugMsg +"GOOD" );
}

/*
 * method: Reset
 *
 * Reset the <AN::LOADINGMUSIC2::g_LoadingMusic>.
 */
void Reset ()
{
    const string szDebugMsg = g_LibString.GetFormattedString( "Reset()...", {} );
    g_LibDebug.verboselogln( szDebugMsg );

    g_LoadingMusic.Stop();
    g_LoadingMusic.ClearTrackList();

    g_LibDebug.logln( szDebugMsg +"GOOD" );
}

/*
 * method: ReadConfigFile
 *
 * Read the config file to load tracks on <AN::LOADINGMUSIC2::g_LoadingMusic>.
 *
 * Arguments:
 * bCanPrecache     - Allow track files to be precached.
 *
 * Returns:
 * true     - Successfully loaded.
 * false    - Failed to load, see debug messages for details.
 */
bool ReadConfigFile (const bool bCanPrecache)
{
    const string szDebugMsg = g_LibString.GetFormattedString( "ReadConfigFile()...", {} );
    g_LibDebug.verboselogln( szDebugMsg );

    const string szConfigDir = g_LibString.GetFormattedString( "%1/%2", {CONFIG_ROOT_PATH,g_LibModule.IsPlugin() ? "plugins" : "maps"} );
    g_LibDebug.verboselogln( g_LibString.GetFormattedString("%1szConfigDir = '%2'", {szDebugMsg, szConfigDir}) );

    bool bUsePrefix;
    string szPath, szBuffer;

    szBuffer = g_Engine.mapname;
    g_LibDebug.verboselogln( g_LibString.GetFormattedString("%1szBuffer = '%2'", {szDebugMsg, szBuffer}) );

    snprintf( szPath, "%1/%2/%3.ini", szConfigDir, CONFIG_MAP_PATH, szBuffer );
    g_LibDebug.logln( g_LibString.GetFormattedString("%1szPath = '%2'", {szDebugMsg, szPath}) );

    if (!g_LibFile.IsExists( szPath ))
    {
        // get map prefix...
        const uint uiFirstUnderscore = szBuffer.FindFirstOf( "_" );
        if (uiFirstUnderscore != String::INVALID_INDEX && uiFirstUnderscore >= 0)
        {
            szBuffer = szBuffer.SubString( 0, uiFirstUnderscore + 1 );
            snprintf( szPath, "%1/%2/%3.ini", szConfigDir, CONFIG_MAP_PATH, szBuffer );
            bUsePrefix = g_LibFile.IsExists( szPath );

            g_LibDebug.verboselogln( g_LibString.GetFormattedString("%1szBuffer = '%2'", {szDebugMsg, szBuffer}) );
            g_LibDebug.logln( g_LibString.GetFormattedString("%1szPath = '%2'", {szDebugMsg, szPath}) );
        }

        g_LibDebug.verboselogln( g_LibString.GetFormattedString("%1bUsePrefix = '%2'", {szDebugMsg, bUsePrefix}) );
        if (!bUsePrefix)
        {
            snprintf( szPath, "%1/%2", szConfigDir, CONFIG_DEFAULT_PATH );
            g_LibDebug.logln( g_LibString.GetFormattedString("%1szPath = '%2'", {szDebugMsg, szPath}) );

            if (!g_LibFile.IsExists( szPath ))
            {
                g_LibDebug.logln( g_LibString.GetFormattedString("%1File not found: '%2'", {szDebugMsg, szPath}) );
                return false;
            }
        }
    }
    else
    {
        bUsePrefix = true;
        g_LibDebug.verboselogln( g_LibString.GetFormattedString("%1bUsePrefix = '%2'", {szDebugMsg, bUsePrefix}) );
    }

    auto@ pFile = g_LibFile.ReadFile( szPath );
    if (pFile is null || !pFile.IsOpen())
    {
        g_LibDebug.logln( g_LibString.GetFormattedString("%1Can't open file: '%2'", {szDebugMsg, szPath}) );
        return false;
    }

    while (!pFile.EOFReached())
    {
        pFile.ReadLine( szBuffer );
        szBuffer.Trim();
        g_LibDebug.verboselogln( g_LibString.GetFormattedString("%1szBuffer = '%2'", {szDebugMsg, szBuffer}) );
        if (szBuffer.IsEmpty()
            || szBuffer[0] == ';'
            || szBuffer[0] == '#'
            || (szBuffer.Length() >= 2 && szBuffer[0] == '/' && szBuffer[1] == '/')
            )
            continue;

        g_LoadingMusic.AddTrack( szBuffer, bCanPrecache );
    }

    pFile.Close();
    g_LibDebug.logln( szDebugMsg +"GOOD" );
    return true;
}

}}
